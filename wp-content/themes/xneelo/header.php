<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package xneelo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://use.typekit.net/ssk8jhx.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'xneelo' ); ?></a>

	<div id="mastside" class="site-nav">
		<div class="site-branding">
			<?php
			the_custom_logo();
            $xneelo_title = get_bloginfo( 'name');
            echo '<span>'.$xneelo_title.'</span>';
            if ( $xneelo_title  ) :
            ?>
            <p class="site-description"><?php echo $xneelo_title; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
            <?php endif;
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$xneelo_description = get_bloginfo( 'description', 'display' );
			if ( $xneelo_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $xneelo_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->
        <nav id="site-navigation" class="main-navigation">
            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'xneelo' ); ?></button>
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                )
            );
            ?>
        </nav><!-- #site-navigation -->
<nav id="logged_navigation">
    <button class="logged-in-menu-toggle" aria-controls="logged-in-menu" aria-expanded="false"><?php esc_html_e( 'User Menu', 'xneelo' ); ?></button>
    <?php
    if(is_user_logged_in()) {
        wp_nav_menu(array(
            'theme_location' => 'logged-in-menu',
            'container_class' => 'logged-menu-class'));
    }
    ?>

</nav>

	</div><!-- #mastside -->
