<?php

/*Custom Post course type start*/
function rk_post_type_courses() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
    );
    $labels = array(
        'name' => _x('courses', 'plural'),
        'singular_name' => _x('course', 'singular'),
        'menu_name' => _x('courses', 'admin menu'),
        'name_admin_bar' => _x('course', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New course'),
        'new_item' => __('New course'),
        'edit_item' => __('Edit course'),
        'view_item' => __('View course'),
        'all_items' => __('All courses'),
        'search_items' => __('Search courses'),
        'not_found' => __('No courses found.'),
    );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'course'),
        'has_archive' => true,
        'hierarchical' => false,
        'show_in_rest' => true,
    );
    register_post_type('course', $args);
}
add_action('init', 'rk_post_type_courses');

function create_course_taxonomies() {

    $skillLabels = array(
        'name' => _x( 'Skill Level', 'taxonomy general name' ),
        'singular_name' => _x( 'Skill Level', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search skill levels' ),
        'all_items' => __( 'All skill levels' ),
        'parent_item' => __( null ),
        'parent_item_colon' => __( null ),
        'edit_item' => __( 'Edit skill level' ),
        'update_item' => __( 'Update skill level' ),
        'add_new_item' => __( 'Add New skill level' ),
        'new_item_name' => __( 'New skill level' ),
        'menu_name' => __( 'Skill Levels' ),
    );

    register_taxonomy(
        'skill',
        'course',
        array(
            'labels' => $skillLabels,
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );

    $durationLabels = array(
        'name' => _x( 'Duration', 'taxonomy general name' ),
        'singular_name' => _x( 'Duration', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Durations' ),
        'all_items' => __( 'All durations' ),
        'parent_item' => __( null ),
        'parent_item_colon' => __( null ),
        'edit_item' => __( 'Edit duration' ),
        'update_item' => __( 'Update duration' ),
        'add_new_item' => __( 'Add New duration' ),
        'new_item_name' => __( 'New duration' ),
        'menu_name' => __( 'Durations' ),
    );

    register_taxonomy(
        'Duration',
        'course',
        array(
            'labels' => $durationLabels,
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );


    $categoryLabels = array(
        'name' => _x( 'course_category', 'taxonomy general name' ),
        'singular_name' => _x( 'Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Categories' ),
        'all_items' => __( 'All categories' ),
        'parent_item' => __( null ),
        'parent_item_colon' => __( null ),
        'edit_item' => __( 'Edit category' ),
        'update_item' => __( 'Update category' ),
        'add_new_item' => __( 'Add New category' ),
        'new_item_name' => __( 'New category' ),
        'menu_name' => __( 'Categories' ),
    );

    register_taxonomy(
        'course_category',
        'course',
        array(
            'labels' => $categoryLabels,
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );

}
add_action( 'init', 'create_course_taxonomies', 0 );


/*Custom Post course type start*/
function rk_post_type_news()
{
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
    );
    $labels = array(
        'name' => _x('news', 'plural'),
        'singular_name' => _x('news', 'singular'),
        'menu_name' => _x('news', 'admin menu'),
        'name_admin_bar' => _x('news', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New news'),
        'new_item' => __('New news'),
        'edit_item' => __('Edit news'),
        'view_item' => __('View news'),
        'all_items' => __('All news'),
        'search_items' => __('Search news'),
        'not_found' => __('No news found.'),
    );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'course'),
        'has_archive' => true,
        'hierarchical' => false,
        'show_in_rest' => true,
    );
    register_post_type('news', $args);
}

add_action('init', 'rk_post_type_news');

function create_news_taxonomies()
{

    $newsLabels = array(
        'name' => _x('news_Category', 'taxonomy general name'),
        'singular_name' => _x('category', 'taxonomy singular name'),
        'search_items' => __('Search categories'),
        'all_items' => __('All categories'),
        'parent_item' => __(null),
        'parent_item_colon' => __(null),
        'edit_item' => __('Edit categories'),
        'update_item' => __('Update categories'),
        'add_new_item' => __('Add New category'),
        'new_item_name' => __('New category'),
        'menu_name' => __('Categories'),
    );

    register_taxonomy(
        'news_category',
        'news',
        array(
            'labels' => $newsLabels,
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );

    $tagLabels = array(
        'name' => _x('Tag', 'taxonomy general name'),
        'singular_name' => _x('Tag', 'taxonomy singular name'),
        'search_items' => __('Search Tags'),
        'all_items' => __('All Tags'),
        'parent_item' => __(null),
        'parent_item_colon' => __(null),
        'edit_item' => __('Edit tag'),
        'update_item' => __('Update tag'),
        'add_new_item' => __('Add New tag'),
        'new_item_name' => __('New tag'),
        'menu_name' => __('Tags'),
    );

    register_taxonomy(
        'Tag',
        'news',
        array(
            'labels' => $tagLabels,
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );


}

add_action('init', 'create_news_taxonomies', 0);