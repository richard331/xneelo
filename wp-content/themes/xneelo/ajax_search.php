
<?php

// Set Hook for outputting JavaScript
add_action('wp_head', 'insert_head_AAPL');


function insert_head_AAPL() {
    //This goes into the header of the site.
    ?>
    <script type="text/javascript">
        checkjQuery = <?php echo get_option('AAPL_jquery_check'); ?>;
        jQueryScriptOutputted = false;

        //Content ID
        var AAPL_content = '<?php echo get_option('AAPL_content_id'); ?>';

        //Search Class
        var AAPL_search_class = '<?php echo get_option('AAPL_search_class'); ?>';

        //Ignore List - this is for travisavery who likes my comments... hello
        var AAPL_ignore_string = new String('<?php echo get_option('AAPL_ignore_list'); ?>');
        var AAPL_ignore = AAPL_ignore_string.split(', ');

        //Shall we take care of analytics?
        var AAPL_track_analytics = <?php echo get_option('AAPL_track_analytics'); ?>

        //Various options and settings
        var AAPL_scroll_top = <?php echo get_option('AAPL_scroll_top'); ?>

        //Maybe the script is being a tw**? With this you can find out why...
        var AAPL_warnings = <?php echo get_option('AAPL_js_debug'); ?>;

        //This is probably not even needed anymore, but lets keep for a fallback
        function initJQuery() {
            if (checkjQuery == true) {
                //if the jQuery object isn't available
                if (typeof(jQuery) == 'undefined') {

                    if (! jQueryScriptOutputted) {
                        //only output the script once..
                        jQueryScriptOutputted = true;

                        //output the jquery script
                        //one day I will complain :/ double quotes inside singles.
                        document.write('<scr' + 'ipt type="text/javascript" src="<?php echo plugins_url( 'jquery.js' , __FILE__ );?>"></scr' + 'ipt>');
                    }
                    setTimeout('initJQuery()', 50);
                }
            }
        }

        initJQuery();

    </script>

    <script type="text/javascript" src="/js/ajax-page-loader.js"></script>


    <script type="text/javascript">
        //urls
        var AAPLsiteurl = "<?php echo get_option('home');?>";
        var AAPLhome = "<?php echo get_option('siteurl');?>";

        //The old code here was RETARDED - Much like the rest of the code... Now I have replaced this with something better ;)
        //PRELOADING YEEEYYYYY!!
        var AAPLloadingIMG = jQuery('<img/>').attr('src', '<?php echo $GLOBALS['AAPLimagesurl'] . '/loaders/' . get_option('AAPL_loading_img') ;?>');
        var AAPLloadingDIV = jQuery('<div/>').attr('style', 'display:none;').attr('id', 'ajaxLoadDivElement');
        AAPLloadingDIV.appendTo('body');
        AAPLloadingIMG.appendTo('#ajaxLoadDivElement');
        //My code can either be seen as sexy? Or just a terribly orchestrated hack? Really it's up to you...

        //Loading/Error Code
        //now using json_encode - two birds one bullet.
        var str = <?php echo json_encode(get_option('AAPL_loading_code')); ?>;
        var AAPL_loading_code = str.replace('{loader}', AAPLloadingIMG.attr('src'));
        str = <?php echo json_encode(get_option('AAPL_loading_error_code')); ?>;
        var AAPL_loading_error_code = str.replace('{loader}', AAPLloadingIMG.attr('src'));
    </script>
    <?php
}