<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package xneelo
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">


		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script>
    jQuery('#statusMenu li').click( function () {

        var filterid = jQuery(this).attr('id');

        if(filterid == 'status-all'){
            jQuery('article').show();
        }
        else{

        jQuery('article').each(function (){

            jQuery(this).show();

            var classes = jQuery(this).attr('class');

            if(!(classes.includes(filterid))){
                jQuery(this).hide();
            }

        });

        }

        jQuery('article').filter(filterid).css('display', 'none');

    });

</script>

</body>
</html>
