<?php
/**
 * Template part for displaying courses
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package xneelo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">

        <?php
        if ( is_singular() ) :
            the_title( '<h1 class="entry-title">', '</h1>' );
        else :
            echo '<span id="dateStyle">'.get_the_date('M').'</span>';
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;
        $categories = get_the_terms( get_the_ID(), 'category' );
        foreach($categories as $category){
            echo '<p class="postArchiveCat">'.$category->name.'</p>';
        }
        if ( 'course' === get_post_type() && is_singular() ) :
            ?>
            <div class="entry-meta">
                <?php
                xneelo_posted_on();
                xneelo_posted_by();
                ?>
            </div><!-- .entry-meta -->
        <?php else: ?>
            <div class="entry-meta">
                <?php
                xneelo_posted_on();
                ?>
            </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->

    <?php xneelo_post_thumbnail(); ?>

    <div class="entry-content">
        <?php
        if(is_singular()){
        the_content(
            sprintf(
                wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'xneelo' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                wp_kses_post( get_the_title() )
            )
        );}
        else {
            the_excerpt(
            sprintf(
                wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'xneelo' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                wp_kses_post( get_the_title() )
            )
        );

        }

        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'xneelo' ),
                'after'  => '</div>',
            )
        );
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php xneelo_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
