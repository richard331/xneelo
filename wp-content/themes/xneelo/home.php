<?php
/**
 * The homepage template file
 *
 * This template is used to show custom code on the homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package xneelo
 */

get_header();
?>

    <main id="primary" class="site-main">

        <header>
            <h1 class="page-title">Courses</h1>

        <?php

            get_search_form(  );
            ?>
</header>
        <div id="courseWrapper">
            <div id="statusMenuWrapper">
        <ul id="statusMenu">
            <li class="active" id="status-all">All</li>
            <li id="status-current">Current</li>
            <li id="status-future">Pending</li>
            <li id="status-completed">Completed</li>
        </ul>
</div>
            <?php
        $args = array('post_type' => 'course',
        'posts_per_page' => 8,
        'orderby' => 'date',
        'order' => 'asc',
            'post_status' => array(
            'future',
            'publish')
        );
        $course_posts = new WP_Query($args);

        if ( $course_posts->have_posts() ) :


            /* Start the Loop */
            while ( $course_posts->have_posts() ) :
                $course_posts->the_post();

                /*
                 * Include the Post-Type-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                 */
                get_template_part( 'template-parts/course-content', get_post_type() );

            endwhile;

            the_posts_navigation();

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>
        </div>
    </main><!-- #main -->

<?php
//get_sidebar();
get_footer();
