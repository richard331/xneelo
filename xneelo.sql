-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 14, 2020 at 01:33 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xneelo`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-07-12 09:06:18', '2020-07-12 09:06:18', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://xneelo.test', 'yes'),
(2, 'home', 'http://xneelo.test', 'yes'),
(3, 'blogname', 'memo', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'richard.klement@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '7', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:126:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:9:\"course/?$\";s:26:\"index.php?post_type=course\";s:39:\"course/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=course&feed=$matches[1]\";s:34:\"course/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=course&feed=$matches[1]\";s:26:\"course/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=course&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:46:\"skill/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?skill=$matches[1]&feed=$matches[2]\";s:41:\"skill/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?skill=$matches[1]&feed=$matches[2]\";s:22:\"skill/([^/]+)/embed/?$\";s:38:\"index.php?skill=$matches[1]&embed=true\";s:34:\"skill/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?skill=$matches[1]&paged=$matches[2]\";s:16:\"skill/([^/]+)/?$\";s:27:\"index.php?skill=$matches[1]\";s:49:\"duration/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?duration=$matches[1]&feed=$matches[2]\";s:44:\"duration/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?duration=$matches[1]&feed=$matches[2]\";s:25:\"duration/([^/]+)/embed/?$\";s:41:\"index.php?duration=$matches[1]&embed=true\";s:37:\"duration/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?duration=$matches[1]&paged=$matches[2]\";s:19:\"duration/([^/]+)/?$\";s:30:\"index.php?duration=$matches[1]\";s:49:\"category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?category=$matches[1]&feed=$matches[2]\";s:44:\"category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?category=$matches[1]&feed=$matches[2]\";s:25:\"category/([^/]+)/embed/?$\";s:41:\"index.php?category=$matches[1]&embed=true\";s:37:\"category/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?category=$matches[1]&paged=$matches[2]\";s:19:\"category/([^/]+)/?$\";s:30:\"index.php?category=$matches[1]\";s:34:\"course/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"course/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"course/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"course/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"course/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"course/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"course/([^/]+)/embed/?$\";s:39:\"index.php?course=$matches[1]&embed=true\";s:27:\"course/([^/]+)/trackback/?$\";s:33:\"index.php?course=$matches[1]&tb=1\";s:47:\"course/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?course=$matches[1]&feed=$matches[2]\";s:42:\"course/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?course=$matches[1]&feed=$matches[2]\";s:35:\"course/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?course=$matches[1]&paged=$matches[2]\";s:42:\"course/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?course=$matches[1]&cpage=$matches[2]\";s:31:\"course/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?course=$matches[1]&page=$matches[2]\";s:23:\"course/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"course/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"course/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"course/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"course/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"course/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:1;s:33:\"classic-editor/classic-editor.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '7', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:56:\"C:\\laragon\\www\\xneelo/wp-content/themes/xneelo/style.css\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'xneelo', 'yes'),
(41, 'stylesheet', 'xneelo', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '0', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1610096778', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:10:{i:1594735579;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1594760778;a:2:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1594760779;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1594803978;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1594804022;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1594804023;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1595235978;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1597840751;a:1:{s:19:\"publish_future_post\";a:1:{s:32:\"4810282cdc4e37bf548c8a4459f8e9ef\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{i:0;i:39;}}}}i:1602247176;a:1:{s:19:\"publish_future_post\";a:1:{s:32:\"2f39f82b6c5c6c7599fc22a165eada38\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{i:0;i:41;}}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:1:{s:22:\"hA4zEZougO7G353E0Q0CId\";a:2:{s:10:\"hashed_key\";s:34:\"$P$B1e7eXUgO56NzchpWKLQ/yqfPP/b1P.\";s:10:\"created_at\";i:1594671623;}}', 'yes'),
(118, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1594545114;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(123, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1594717899;s:15:\"version_checked\";s:5:\"5.4.2\";s:12:\"translations\";a:0:{}}', 'no'),
(126, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:25:\"richard.klement@gmail.com\";s:7:\"version\";s:5:\"5.4.2\";s:9:\"timestamp\";i:1594544789;}', 'no'),
(128, '_site_transient_timeout_browser_6aa22f8afb172793f3803c076764d327', '1595149623', 'no'),
(129, '_site_transient_browser_6aa22f8afb172793f3803c076764d327', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(130, '_site_transient_timeout_php_check_0cbcbda5109bcde6b94054595b5c2163', '1595149623', 'no'),
(131, '_site_transient_php_check_0cbcbda5109bcde6b94054595b5c2163', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(135, 'can_compress_scripts', '1', 'no'),
(146, 'current_theme', 'xneelo', 'yes'),
(147, 'theme_mods_xneelo', 'a:5:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:6:\"menu-1\";i:8;s:14:\"logged-in-menu\";i:9;}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:6;s:16:\"header_textcolor\";s:5:\"blank\";}', 'yes'),
(148, 'theme_switched', '', 'yes'),
(153, 'recovery_mode_email_last_sent', '1594671623', 'yes'),
(160, 'recently_activated', 'a:1:{s:55:\"advanced-ajax-page-loader/advanced-ajax-page-loader.php\";i:1594708876;}', 'yes'),
(164, 'Status_children', 'a:0:{}', 'yes'),
(178, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(179, 'classic-editor-replace', 'classic', 'yes'),
(180, 'classic-editor-allow-users', 'disallow', 'yes'),
(187, 'category_children', 'a:0:{}', 'yes'),
(191, 'course_category_children', 'a:0:{}', 'yes'),
(192, 'skill_children', 'a:0:{}', 'yes'),
(193, 'Duration_children', 'a:0:{}', 'yes'),
(203, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1594717899;s:7:\"checked\";a:4:{s:14:\"twentynineteen\";s:3:\"1.5\";s:15:\"twentyseventeen\";s:3:\"2.3\";s:12:\"twentytwenty\";s:3:\"1.2\";s:6:\"xneelo\";s:5:\"1.0.0\";}s:8:\"response\";a:2:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.6.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.4.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(205, '_transient_health-check-site-status-result', '{\"good\":\"7\",\"recommended\":\"10\",\"critical\":\"0\"}', 'yes'),
(218, 'AAPL_content_id', 'content', 'yes'),
(219, 'AAPL_search_class', 'searchform', 'yes'),
(220, 'AAPL_version', '2.7.7', 'yes'),
(221, 'AAPL_loading_img', 'WordPress Ball Spin.gif', 'yes'),
(222, 'AAPL_js_debug', 'false', 'yes'),
(223, 'AAPL_sponsor', 'false', 'yes'),
(224, 'AAPL_commercial', 'false', 'yes'),
(225, 'AAPL_jquery_check', 'false', 'yes'),
(226, 'AAPL_track_analytics', 'false', 'yes'),
(227, 'AAPL_scroll_top', 'true', 'yes'),
(228, 'AAPL_reload_code', '', 'yes'),
(229, 'AAPL_data_code', '', 'yes'),
(230, 'AAPL_click_code', '// highlight the current menu item\njQuery(\'ul.menu li\').each(function() {\n	jQuery(this).removeClass(\'current-menu-item\');\n});\njQuery(thiss).parents(\'li\').addClass(\'current-menu-item\');', 'yes'),
(231, 'AAPL_loading_code', '<center>\n	<p style=\"text-align: center !important;\">Loading... Please Wait...</p>\n	<p style=\"text-align: center !important;\">\n		<img src=\"{loader}\" border=\"0\" alt=\"Loading Image\" title=\"Please Wait...\" />\n	</p>\n</center>', 'yes'),
(232, 'AAPL_loading_error_code', '<center>\n	<p style=\"text-align: center !important;\">Error!</p>\n	<p style=\"text-align: center !important;\">\n		<font color=\"red\">There was a problem and the page didnt load.</font>\n	</p>\n</center>', 'yes'),
(233, 'AAPL_ignore_list', '#, /wp-, .pdf, .zip, .rar', 'yes'),
(234, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1594717899;s:7:\"checked\";a:4:{s:55:\"advanced-ajax-page-loader/advanced-ajax-page-loader.php\";s:5:\"2.7.7\";s:19:\"akismet/akismet.php\";s:5:\"4.1.5\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.5\";s:9:\"hello.php\";s:5:\"1.7.2\";}s:8:\"response\";a:1:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:55:\"advanced-ajax-page-loader/advanced-ajax-page-loader.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/advanced-ajax-page-loader\";s:4:\"slug\";s:25:\"advanced-ajax-page-loader\";s:6:\"plugin\";s:55:\"advanced-ajax-page-loader/advanced-ajax-page-loader.php\";s:11:\"new_version\";s:5:\"2.7.7\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/advanced-ajax-page-loader/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/advanced-ajax-page-loader.2.7.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-ajax-page-loader/assets/icon-256x256.png?rev=1286289\";s:2:\"1x\";s:78:\"https://ps.w.org/advanced-ajax-page-loader/assets/icon-128x128.png?rev=1286289\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:80:\"https://ps.w.org/advanced-ajax-page-loader/assets/banner-772x250.png?rev=1286288\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(235, 'AAPL_upload_error', '', 'yes'),
(242, '_site_transient_timeout_theme_roots', '1594719699', 'no'),
(243, '_site_transient_theme_roots', 'a:4:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:6:\"xneelo\";s:7:\"/themes\";}', 'no'),
(244, '_transient_timeout_dash_v2_88ae138922fe95674369b1cb3d215a2b', '1594762058', 'no'),
(245, '_transient_dash_v2_88ae138922fe95674369b1cb3d215a2b', '<div class=\"rss-widget\"><p><strong>RSS Error:</strong> WP HTTP Error: A valid URL was not provided.</p></div><div class=\"rss-widget\"><p><strong>RSS Error:</strong> WP HTTP Error: A valid URL was not provided.</p></div>', 'no'),
(250, 'news_category_children', 'a:0:{}', 'yes'),
(251, 'Tag_children', 'a:0:{}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2020/07/Capture.jpg'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:43;s:6:\"height\";i:44;s:4:\"file\";s:19:\"2020/07/Capture.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 6, '_wp_attached_file', '2020/07/cropped-Capture.jpg'),
(6, 6, '_wp_attachment_context', 'custom-logo'),
(7, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:43;s:6:\"height\";i:43;s:4:\"file\";s:27:\"2020/07/cropped-Capture.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(8, 7, '_edit_lock', '1594546910:1'),
(9, 7, '_wp_trash_meta_status', 'publish'),
(10, 7, '_wp_trash_meta_time', '1594546958'),
(11, 1, '_edit_lock', '1594555872:1'),
(12, 25, '_edit_lock', '1594557385:1'),
(13, 37, '_edit_last', '1'),
(14, 37, '_edit_lock', '1594719809:1'),
(15, 39, '_edit_last', '1'),
(16, 39, '_edit_lock', '1594566453:1'),
(17, 41, '_edit_last', '1'),
(18, 41, '_edit_lock', '1594566448:1'),
(19, 41, '_wp_old_slug', 'test3'),
(20, 45, '_wp_attached_file', '2020/07/78c4ba626f026637b8e44637527af9ce_05.jpg'),
(21, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:283;s:6:\"height\";i:153;s:4:\"file\";s:47:\"2020/07/78c4ba626f026637b8e44637527af9ce_05.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"78c4ba626f026637b8e44637527af9ce_05-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(22, 39, '_thumbnail_id', '45'),
(23, 39, '_wp_old_slug', 'test2'),
(24, 37, '_wp_old_slug', 'test'),
(25, 37, '_wp_old_date', '2020-07-12'),
(26, 49, '_wp_attached_file', '2020/05/78c4ba626f026637b8e44637527af9ce_03.jpg'),
(27, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:285;s:6:\"height\";i:153;s:4:\"file\";s:47:\"2020/05/78c4ba626f026637b8e44637527af9ce_03.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"78c4ba626f026637b8e44637527af9ce_03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(28, 37, '_thumbnail_id', '82'),
(29, 50, '_wp_attached_file', '2020/10/78c4ba626f026637b8e44637527af9ce_07.jpg'),
(30, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:285;s:6:\"height\";i:153;s:4:\"file\";s:47:\"2020/10/78c4ba626f026637b8e44637527af9ce_07.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"78c4ba626f026637b8e44637527af9ce_07-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 41, '_thumbnail_id', '50'),
(32, 55, '_menu_item_type', 'custom'),
(33, 55, '_menu_item_menu_item_parent', '0'),
(34, 55, '_menu_item_object_id', '55'),
(35, 55, '_menu_item_object', 'custom'),
(36, 55, '_menu_item_target', ''),
(37, 55, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(38, 55, '_menu_item_xfn', ''),
(39, 55, '_menu_item_url', 'http://xneelo.test/'),
(40, 55, '_menu_item_orphaned', '1594564152'),
(41, 56, '_menu_item_type', 'post_type'),
(42, 56, '_menu_item_menu_item_parent', '0'),
(43, 56, '_menu_item_object_id', '2'),
(44, 56, '_menu_item_object', 'page'),
(45, 56, '_menu_item_target', ''),
(46, 56, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(47, 56, '_menu_item_xfn', ''),
(48, 56, '_menu_item_url', ''),
(49, 56, '_menu_item_orphaned', '1594564152'),
(50, 57, '_edit_last', '1'),
(51, 57, '_edit_lock', '1594719892:1'),
(52, 59, '_edit_last', '1'),
(53, 59, '_edit_lock', '1594719856:1'),
(54, 61, '_edit_last', '1'),
(55, 61, '_edit_lock', '1594719872:1'),
(56, 63, '_edit_last', '1'),
(57, 63, '_edit_lock', '1594719944:1'),
(58, 65, '_edit_last', '1'),
(59, 65, '_edit_lock', '1594719964:1'),
(60, 67, '_menu_item_type', 'custom'),
(61, 67, '_menu_item_menu_item_parent', '0'),
(62, 67, '_menu_item_object_id', '67'),
(63, 67, '_menu_item_object', 'custom'),
(64, 67, '_menu_item_target', ''),
(65, 67, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(66, 67, '_menu_item_xfn', ''),
(67, 67, '_menu_item_url', 'http://xneelo.test/'),
(68, 67, '_menu_item_orphaned', '1594564229'),
(69, 68, '_menu_item_type', 'post_type'),
(70, 68, '_menu_item_menu_item_parent', '0'),
(71, 68, '_menu_item_object_id', '59'),
(72, 68, '_menu_item_object', 'page'),
(73, 68, '_menu_item_target', ''),
(74, 68, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(75, 68, '_menu_item_xfn', ''),
(76, 68, '_menu_item_url', ''),
(78, 69, '_menu_item_type', 'post_type'),
(79, 69, '_menu_item_menu_item_parent', '0'),
(80, 69, '_menu_item_object_id', '61'),
(81, 69, '_menu_item_object', 'page'),
(82, 69, '_menu_item_target', ''),
(83, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(84, 69, '_menu_item_xfn', ''),
(85, 69, '_menu_item_url', ''),
(87, 70, '_menu_item_type', 'post_type'),
(88, 70, '_menu_item_menu_item_parent', '0'),
(89, 70, '_menu_item_object_id', '57'),
(90, 70, '_menu_item_object', 'page'),
(91, 70, '_menu_item_target', ''),
(92, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(93, 70, '_menu_item_xfn', ''),
(94, 70, '_menu_item_url', ''),
(96, 71, '_menu_item_type', 'post_type'),
(97, 71, '_menu_item_menu_item_parent', '0'),
(98, 71, '_menu_item_object_id', '2'),
(99, 71, '_menu_item_object', 'page'),
(100, 71, '_menu_item_target', ''),
(101, 71, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(102, 71, '_menu_item_xfn', ''),
(103, 71, '_menu_item_url', ''),
(104, 71, '_menu_item_orphaned', '1594564229'),
(105, 72, '_menu_item_type', 'post_type'),
(106, 72, '_menu_item_menu_item_parent', '0'),
(107, 72, '_menu_item_object_id', '63'),
(108, 72, '_menu_item_object', 'page'),
(109, 72, '_menu_item_target', ''),
(110, 72, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(111, 72, '_menu_item_xfn', ''),
(112, 72, '_menu_item_url', ''),
(114, 73, '_menu_item_type', 'post_type'),
(115, 73, '_menu_item_menu_item_parent', '0'),
(116, 73, '_menu_item_object_id', '65'),
(117, 73, '_menu_item_object', 'page'),
(118, 73, '_menu_item_target', ''),
(119, 73, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(120, 73, '_menu_item_xfn', ''),
(121, 73, '_menu_item_url', ''),
(123, 74, '_menu_item_type', 'custom'),
(124, 74, '_menu_item_menu_item_parent', '0'),
(125, 74, '_menu_item_object_id', '74'),
(126, 74, '_menu_item_object', 'custom'),
(127, 74, '_menu_item_target', ''),
(128, 74, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(129, 74, '_menu_item_xfn', ''),
(130, 74, '_menu_item_url', '/'),
(132, 75, '_edit_last', '1'),
(133, 75, '_edit_lock', '1594719836:1'),
(134, 77, '_edit_last', '1'),
(135, 77, '_edit_lock', '1594719926:1'),
(136, 79, '_menu_item_type', 'post_type'),
(137, 79, '_menu_item_menu_item_parent', '0'),
(138, 79, '_menu_item_object_id', '77'),
(139, 79, '_menu_item_object', 'page'),
(140, 79, '_menu_item_target', ''),
(141, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(142, 79, '_menu_item_xfn', ''),
(143, 79, '_menu_item_url', ''),
(145, 80, '_menu_item_type', 'post_type'),
(146, 80, '_menu_item_menu_item_parent', '0'),
(147, 80, '_menu_item_object_id', '75'),
(148, 80, '_menu_item_object', 'page'),
(149, 80, '_menu_item_target', ''),
(150, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(151, 80, '_menu_item_xfn', ''),
(152, 80, '_menu_item_url', ''),
(154, 1, '_wp_trash_meta_status', 'publish'),
(155, 1, '_wp_trash_meta_time', '1594565670'),
(156, 1, '_wp_desired_post_slug', 'hello-world'),
(157, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(158, 82, '_wp_attached_file', '2020/05/78c4ba626f026637b8e44637527af9ce_03-1.jpg'),
(159, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:285;s:6:\"height\";i:153;s:4:\"file\";s:49:\"2020/05/78c4ba626f026637b8e44637527af9ce_03-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:49:\"78c4ba626f026637b8e44637527af9ce_03-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(162, 85, '_edit_last', '1'),
(163, 85, '_edit_lock', '1594720075:1'),
(164, 87, '_edit_last', '1'),
(165, 87, '_edit_lock', '1594720088:1'),
(166, 88, '_wp_attached_file', '2020/07/creation.jpg'),
(167, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:399;s:4:\"file\";s:20:\"2020/07/creation.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"creation-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"creation-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"creation-768x306.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:306;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(168, 87, '_thumbnail_id', '88'),
(169, 97, '_wp_attached_file', '2020/07/skulls.jpg'),
(170, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:707;s:4:\"file\";s:18:\"2020/07/skulls.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"skulls-300x212.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"skulls-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"skulls-768x543.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:543;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(171, 85, '_thumbnail_id', '97'),
(172, 99, '_edit_last', '1'),
(173, 99, '_edit_lock', '1594731244:1'),
(174, 101, '_wp_attached_file', '2020/07/wolf-bell.jpg'),
(175, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:700;s:6:\"height\";i:519;s:4:\"file\";s:21:\"2020/07/wolf-bell.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"wolf-bell-300x222.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:222;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"wolf-bell-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(176, 99, '_thumbnail_id', '101');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-07-12 09:06:18', '2020-07-12 09:06:18', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2020-07-12 14:54:30', '2020-07-12 14:54:30', '', 0, 'http://xneelo.test/?p=1', 0, 'post', '', 1),
(2, 1, '2020-07-12 09:06:18', '2020-07-12 09:06:18', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://xneelo.test/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-07-12 09:06:18', '2020-07-12 09:06:18', '', 0, 'http://xneelo.test/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-07-12 09:06:18', '2020-07-12 09:06:18', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://xneelo.test.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-07-12 09:06:18', '2020-07-12 09:06:18', '', 0, 'http://xneelo.test/?page_id=3', 0, 'page', '', 0),
(4, 1, '2020-07-12 09:07:03', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-07-12 09:07:03', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=4', 0, 'post', '', 0),
(5, 1, '2020-07-12 09:23:56', '2020-07-12 09:23:56', '', 'Capture', '', 'inherit', 'open', 'closed', '', 'capture', '', '', '2020-07-12 09:23:56', '2020-07-12 09:23:56', '', 0, 'http://xneelo.test/wp-content/uploads/2020/07/Capture.jpg', 0, 'attachment', 'image/jpeg', 0),
(6, 1, '2020-07-12 09:24:03', '2020-07-12 09:24:03', 'http://xneelo.test/wp-content/uploads/2020/07/cropped-Capture.jpg', 'cropped-Capture.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-capture-jpg', '', '', '2020-07-12 09:24:03', '2020-07-12 09:24:03', '', 0, 'http://xneelo.test/wp-content/uploads/2020/07/cropped-Capture.jpg', 0, 'attachment', 'image/jpeg', 0),
(7, 1, '2020-07-12 09:42:38', '2020-07-12 09:42:38', '{\n    \"blogname\": {\n        \"value\": \"memo\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-07-12 09:25:03\"\n    },\n    \"xneelo::custom_logo\": {\n        \"value\": 6,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-07-12 09:25:03\"\n    },\n    \"xneelo::header_textcolor\": {\n        \"value\": \"blank\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-07-12 09:25:03\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '8306e63e-e02c-48c9-9cd6-a4c0366f81bf', '', '', '2020-07-12 09:42:38', '2020-07-12 09:42:38', '', 0, 'http://xneelo.test/?p=7', 0, 'customize_changeset', '', 0),
(11, 1, '2020-07-12 11:22:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:22:37', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=11', 0, 'courses', '', 0),
(12, 1, '2020-07-12 11:23:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:23:26', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=12', 0, 'courses', '', 0),
(13, 1, '2020-07-12 11:24:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:24:32', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=13', 0, 'courses', '', 0),
(14, 1, '2020-07-12 11:31:30', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:31:30', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=14', 0, 'courses', '', 0),
(15, 1, '2020-07-12 11:32:25', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:32:25', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=15', 0, 'courses', '', 0),
(16, 1, '2020-07-12 11:33:30', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:33:30', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=16', 0, 'courses', '', 0),
(17, 1, '2020-07-12 11:35:06', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:35:06', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=17', 0, 'courses', '', 0),
(18, 1, '2020-07-12 11:36:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:36:19', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=18', 0, 'courses', '', 0),
(19, 1, '2020-07-12 11:36:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:36:41', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=19', 0, 'courses', '', 0),
(20, 1, '2020-07-12 11:50:20', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:50:20', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=20', 0, 'courses', '', 0),
(21, 1, '2020-07-12 11:50:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:50:51', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=21', 0, 'courses', '', 0),
(22, 1, '2020-07-12 11:50:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:50:53', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=22', 0, 'courses', '', 0),
(23, 1, '2020-07-12 11:51:45', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:51:45', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=23', 0, 'courses', '', 0),
(24, 1, '2020-07-12 11:53:01', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 11:53:01', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=courses&p=24', 0, 'courses', '', 0),
(25, 1, '2020-07-12 11:56:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-07-12 11:56:54', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=25', 0, 'post', '', 0),
(26, 1, '2020-07-12 11:58:14', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-07-12 11:58:14', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=26', 0, 'post', '', 0),
(27, 1, '2020-07-12 11:59:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-07-12 11:59:37', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=27', 0, 'post', '', 0),
(28, 1, '2020-07-12 11:59:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-07-12 11:59:50', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=28', 0, 'post', '', 0),
(34, 1, '2020-07-12 12:13:40', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 12:13:40', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=course&p=34', 0, 'course', '', 0),
(35, 1, '2020-07-12 12:13:58', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 12:13:58', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=course&p=35', 0, 'course', '', 0),
(36, 1, '2020-07-12 12:14:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-07-12 12:14:26', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?post_type=course&p=36', 0, 'course', '', 0),
(37, 1, '2020-05-17 12:19:47', '2020-05-17 12:19:47', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Visual Elements of User Interface Design', '', 'publish', 'open', 'closed', '', 'visual-elements-of-user-interface-design', '', '', '2020-07-13 18:06:00', '2020-07-13 18:06:00', '', 0, 'http://xneelo.test/?post_type=course&#038;p=37', 0, 'course', '', 0),
(38, 1, '2020-07-12 12:19:47', '2020-07-12 12:19:47', '', 'test', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2020-07-12 12:19:47', '2020-07-12 12:19:47', '', 37, 'http://xneelo.test/2020/07/12/37-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2020-08-19 12:39:11', '2020-08-19 12:39:11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Object Oriented Programming in Java', '', 'future', 'open', 'closed', '', 'object-oriented-programming-in-java', '', '', '2020-07-12 15:07:33', '2020-07-12 15:07:33', '', 0, 'http://xneelo.test/?post_type=course&#038;p=39', 0, 'course', '', 0),
(40, 1, '2020-07-12 12:39:11', '2020-07-12 12:39:11', '', 'test2', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-07-12 12:39:11', '2020-07-12 12:39:11', '', 39, 'http://xneelo.test/2020/07/12/39-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2020-10-09 12:39:36', '2020-10-09 12:39:36', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'The Strategy of Content Marketing', '', 'future', 'open', 'closed', '', 'the-strategy-of-content-marketing', '', '', '2020-07-12 15:07:28', '2020-07-12 15:07:28', '', 0, 'http://xneelo.test/?post_type=course&#038;p=41', 0, 'course', '', 0),
(42, 1, '2020-07-12 12:39:36', '2020-07-12 12:39:36', '', 'test3', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2020-07-12 12:39:36', '2020-07-12 12:39:36', '', 41, 'http://xneelo.test/2020/07/12/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2020-07-12 13:34:20', '2020-07-12 13:34:20', '', 'The Strategy of Content Marketing', '', 'inherit', 'closed', 'closed', '', '41-autosave-v1', '', '', '2020-07-12 13:34:20', '2020-07-12 13:34:20', '', 41, 'http://xneelo.test/2020/07/12/41-autosave-v1/', 0, 'revision', '', 0),
(44, 1, '2020-07-12 13:38:06', '2020-07-12 13:38:06', '', 'The Strategy of Content Marketing', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2020-07-12 13:38:06', '2020-07-12 13:38:06', '', 41, 'http://xneelo.test/2020/07/12/41-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-07-12 13:39:33', '2020-07-12 13:39:33', '', '78c4ba626f026637b8e44637527af9ce_05', '', 'inherit', 'open', 'closed', '', '78c4ba626f026637b8e44637527af9ce_05', '', '', '2020-07-12 13:39:33', '2020-07-12 13:39:33', '', 39, 'http://xneelo.test/wp-content/uploads/2020/07/78c4ba626f026637b8e44637527af9ce_05.jpg', 0, 'attachment', 'image/jpeg', 0),
(46, 1, '2020-07-12 13:39:37', '2020-07-12 13:39:37', '', 'Bject Oriented Programming in Java', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-07-12 13:39:37', '2020-07-12 13:39:37', '', 39, 'http://xneelo.test/2020/07/12/39-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2020-07-12 13:42:23', '2020-07-12 13:42:23', '', 'Visual Elements of User Interface Design', '', 'inherit', 'closed', 'closed', '', '37-autosave-v1', '', '', '2020-07-12 13:42:23', '2020-07-12 13:42:23', '', 37, 'http://xneelo.test/2020/07/12/37-autosave-v1/', 0, 'revision', '', 0),
(48, 1, '2020-07-12 13:42:24', '2020-07-12 13:42:24', '', 'Visual Elements of User Interface Design', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2020-07-12 13:42:24', '2020-07-12 13:42:24', '', 37, 'http://xneelo.test/2020/07/12/37-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2020-07-12 13:42:39', '2020-07-12 13:42:39', '', '78c4ba626f026637b8e44637527af9ce_03', '', 'inherit', 'open', 'closed', '', '78c4ba626f026637b8e44637527af9ce_03', '', '', '2020-07-12 13:42:39', '2020-07-12 13:42:39', '', 37, 'http://xneelo.test/wp-content/uploads/2020/05/78c4ba626f026637b8e44637527af9ce_03.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2020-07-12 13:42:59', '2020-07-12 13:42:59', '', '78c4ba626f026637b8e44637527af9ce_07', '', 'inherit', 'open', 'closed', '', '78c4ba626f026637b8e44637527af9ce_07', '', '', '2020-07-12 13:42:59', '2020-07-12 13:42:59', '', 41, 'http://xneelo.test/wp-content/uploads/2020/10/78c4ba626f026637b8e44637527af9ce_07.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2020-07-12 13:43:25', '2020-07-12 13:43:25', '', 'Object Oriented Programming in Java', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-07-12 13:43:25', '2020-07-12 13:43:25', '', 39, 'http://xneelo.test/2020/07/12/39-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2020-07-12 14:07:53', '2020-07-12 14:07:53', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Visual Elements of User Interface Design', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2020-07-12 14:07:53', '2020-07-12 14:07:53', '', 37, 'http://xneelo.test/2020/07/12/37-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2020-07-12 14:08:04', '2020-07-12 14:08:04', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Object Oriented Programming in Java', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-07-12 14:08:04', '2020-07-12 14:08:04', '', 39, 'http://xneelo.test/2020/07/12/39-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2020-07-12 14:08:12', '2020-07-12 14:08:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'The Strategy of Content Marketing', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2020-07-12 14:08:12', '2020-07-12 14:08:12', '', 41, 'http://xneelo.test/2020/07/12/41-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2020-07-12 14:29:12', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-07-12 14:29:12', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=55', 1, 'nav_menu_item', '', 0),
(56, 1, '2020-07-12 14:29:12', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-07-12 14:29:12', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=56', 1, 'nav_menu_item', '', 0),
(57, 1, '2020-07-12 14:29:30', '2020-07-12 14:29:30', '<h2>Payment</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Payment', '', 'publish', 'closed', 'closed', '', 'payment', '', '', '2020-07-14 09:47:14', '2020-07-14 09:47:14', '', 0, 'http://xneelo.test/?page_id=57', 0, 'page', '', 0),
(58, 1, '2020-07-12 14:29:30', '2020-07-12 14:29:30', '', 'Payment', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2020-07-12 14:29:30', '2020-07-12 14:29:30', '', 57, 'http://xneelo.test/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2020-07-12 14:29:39', '2020-07-12 14:29:39', '<h2>Chat</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Chat', '', 'publish', 'closed', 'closed', '', 'chat', '', '', '2020-07-14 09:46:39', '2020-07-14 09:46:39', '', 0, 'http://xneelo.test/?page_id=59', 0, 'page', '', 0),
(60, 1, '2020-07-12 14:29:39', '2020-07-12 14:29:39', '', 'Chat', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-07-12 14:29:39', '2020-07-12 14:29:39', '', 59, 'http://xneelo.test/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2020-07-12 14:29:49', '2020-07-12 14:29:49', '<h2>Groups</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Groups', '', 'publish', 'closed', 'closed', '', 'groups', '', '', '2020-07-14 09:46:55', '2020-07-14 09:46:55', '', 0, 'http://xneelo.test/?page_id=61', 0, 'page', '', 0),
(62, 1, '2020-07-12 14:29:49', '2020-07-12 14:29:49', '', 'Groups', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2020-07-12 14:29:49', '2020-07-12 14:29:49', '', 61, 'http://xneelo.test/61-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2020-07-12 14:29:58', '2020-07-12 14:29:58', '<h2>Students</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Students', '', 'publish', 'closed', 'closed', '', 'students', '', '', '2020-07-14 09:48:06', '2020-07-14 09:48:06', '', 0, 'http://xneelo.test/?page_id=63', 0, 'page', '', 0),
(64, 1, '2020-07-12 14:29:58', '2020-07-12 14:29:58', '', 'Students', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2020-07-12 14:29:58', '2020-07-12 14:29:58', '', 63, 'http://xneelo.test/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2020-07-12 14:30:07', '2020-07-12 14:30:07', '<h2>Support</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Support', '', 'publish', 'closed', 'closed', '', 'support', '', '', '2020-07-14 09:48:25', '2020-07-14 09:48:25', '', 0, 'http://xneelo.test/?page_id=65', 0, 'page', '', 0),
(66, 1, '2020-07-12 14:30:07', '2020-07-12 14:30:07', '', 'Support', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2020-07-12 14:30:07', '2020-07-12 14:30:07', '', 65, 'http://xneelo.test/65-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2020-07-12 14:30:29', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-07-12 14:30:29', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=67', 1, 'nav_menu_item', '', 0),
(68, 1, '2020-07-12 14:31:41', '2020-07-12 14:31:41', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 0, 'http://xneelo.test/?p=68', 3, 'nav_menu_item', '', 0),
(69, 1, '2020-07-12 14:31:41', '2020-07-12 14:31:41', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 0, 'http://xneelo.test/?p=69', 4, 'nav_menu_item', '', 0),
(70, 1, '2020-07-12 14:31:41', '2020-07-12 14:31:41', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 0, 'http://xneelo.test/?p=70', 2, 'nav_menu_item', '', 0),
(71, 1, '2020-07-12 14:30:29', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-07-12 14:30:29', '0000-00-00 00:00:00', '', 0, 'http://xneelo.test/?p=71', 1, 'nav_menu_item', '', 0),
(72, 1, '2020-07-12 14:31:41', '2020-07-12 14:31:41', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 0, 'http://xneelo.test/?p=72', 5, 'nav_menu_item', '', 0),
(73, 1, '2020-07-12 14:31:41', '2020-07-12 14:31:41', ' ', '', '', 'publish', 'closed', 'closed', '', '73', '', '', '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 0, 'http://xneelo.test/?p=73', 6, 'nav_menu_item', '', 0),
(74, 1, '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 'Courses', '', 'publish', 'closed', 'closed', '', 'courses', '', '', '2020-07-12 14:31:41', '2020-07-12 14:31:41', '', 0, 'http://xneelo.test/?p=74', 1, 'nav_menu_item', '', 0),
(75, 1, '2020-07-12 14:36:56', '2020-07-12 14:36:56', '<h2>Account</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Account', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2020-07-14 09:46:19', '2020-07-14 09:46:19', '', 0, 'http://xneelo.test/?page_id=75', 0, 'page', '', 0),
(76, 1, '2020-07-12 14:36:56', '2020-07-12 14:36:56', '', 'Account', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2020-07-12 14:36:56', '2020-07-12 14:36:56', '', 75, 'http://xneelo.test/75-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2020-07-12 14:37:06', '2020-07-12 14:37:06', '<h2>Settings</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Settings', '', 'publish', 'closed', 'closed', '', 'settings', '', '', '2020-07-14 09:47:48', '2020-07-14 09:47:48', '', 0, 'http://xneelo.test/?page_id=77', 0, 'page', '', 0),
(78, 1, '2020-07-12 14:37:06', '2020-07-12 14:37:06', '', 'Settings', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2020-07-12 14:37:06', '2020-07-12 14:37:06', '', 77, 'http://xneelo.test/77-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2020-07-12 14:41:14', '2020-07-12 14:41:14', ' ', '', '', 'publish', 'closed', 'closed', '', '79', '', '', '2020-07-12 14:41:14', '2020-07-12 14:41:14', '', 0, 'http://xneelo.test/?p=79', 2, 'nav_menu_item', '', 0),
(80, 1, '2020-07-12 14:41:14', '2020-07-12 14:41:14', ' ', '', '', 'publish', 'closed', 'closed', '', '80', '', '', '2020-07-12 14:41:14', '2020-07-12 14:41:14', '', 0, 'http://xneelo.test/?p=80', 1, 'nav_menu_item', '', 0),
(81, 1, '2020-07-12 14:54:30', '2020-07-12 14:54:30', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-07-12 14:54:30', '2020-07-12 14:54:30', '', 1, 'http://xneelo.test/1-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2020-07-13 18:05:46', '2020-07-13 18:05:46', '', '78c4ba626f026637b8e44637527af9ce_03', '', 'inherit', 'open', 'closed', '', '78c4ba626f026637b8e44637527af9ce_03-2', '', '', '2020-07-13 18:05:46', '2020-07-13 18:05:46', '', 37, 'http://xneelo.test/wp-content/uploads/2020/05/78c4ba626f026637b8e44637527af9ce_03-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2020-07-13 19:59:59', '2020-07-13 19:59:59', 'test', 'Chat', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-07-13 19:59:59', '2020-07-13 19:59:59', '', 59, 'http://xneelo.test/59-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2020-07-14 09:50:02', '2020-07-14 09:50:02', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Loadshedding', '', 'publish', 'open', 'closed', '', 'loadshedding', '', '', '2020-07-14 09:50:18', '2020-07-14 09:50:18', '', 0, 'http://xneelo.test/?post_type=news&#038;p=85', 0, 'news', '', 0),
(87, 1, '2020-07-14 03:54:26', '2020-07-14 03:54:26', 'Dummy content', 'Trouble in Paradise', '', 'publish', 'open', 'closed', '', 'trouble-in-paradise', '', '', '2020-07-14 09:50:28', '2020-07-14 09:50:28', '', 0, 'http://xneelo.test/?post_type=news&#038;p=87', 0, 'news', '', 0),
(88, 1, '2020-07-14 03:54:14', '2020-07-14 03:54:14', '', 'creation', '', 'inherit', 'open', 'closed', '', 'creation', '', '', '2020-07-14 03:54:14', '2020-07-14 03:54:14', '', 87, 'http://xneelo.test/wp-content/uploads/2020/07/creation.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2020-07-14 03:54:26', '2020-07-14 03:54:26', 'Dummy content', 'Trouble in Paradise', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2020-07-14 03:54:26', '2020-07-14 03:54:26', '', 87, 'http://xneelo.test/87-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2020-07-14 09:46:19', '2020-07-14 09:46:19', '<h2>Account</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Account', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2020-07-14 09:46:19', '2020-07-14 09:46:19', '', 75, 'http://xneelo.test/75-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2020-07-14 09:46:39', '2020-07-14 09:46:39', '<h2>Chat</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Chat', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-07-14 09:46:39', '2020-07-14 09:46:39', '', 59, 'http://xneelo.test/59-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2020-07-14 09:46:55', '2020-07-14 09:46:55', '<h2>Groups</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Groups', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2020-07-14 09:46:55', '2020-07-14 09:46:55', '', 61, 'http://xneelo.test/61-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2020-07-14 09:47:14', '2020-07-14 09:47:14', '<h2>Payment</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Payment', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2020-07-14 09:47:14', '2020-07-14 09:47:14', '', 57, 'http://xneelo.test/57-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2020-07-14 09:47:48', '2020-07-14 09:47:48', '<h2>Settings</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Settings', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2020-07-14 09:47:48', '2020-07-14 09:47:48', '', 77, 'http://xneelo.test/77-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2020-07-14 09:48:06', '2020-07-14 09:48:06', '<h2>Students</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Students', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2020-07-14 09:48:06', '2020-07-14 09:48:06', '', 63, 'http://xneelo.test/63-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2020-07-14 09:48:25', '2020-07-14 09:48:25', '<h2>Support</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Support', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2020-07-14 09:48:25', '2020-07-14 09:48:25', '', 65, 'http://xneelo.test/65-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2020-07-14 09:49:53', '2020-07-14 09:49:53', '', 'skulls', '', 'inherit', 'open', 'closed', '', 'skulls', '', '', '2020-07-14 09:49:53', '2020-07-14 09:49:53', '', 85, 'http://xneelo.test/wp-content/uploads/2020/07/skulls.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2020-07-14 09:50:02', '2020-07-14 09:50:02', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Loadshedding', '', 'inherit', 'closed', 'closed', '', '85-revision-v1', '', '', '2020-07-14 09:50:02', '2020-07-14 09:50:02', '', 85, 'http://xneelo.test/85-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2020-07-14 09:51:09', '2020-07-14 09:51:09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Virus', '', 'publish', 'open', 'closed', '', 'virus', '', '', '2020-07-14 09:51:50', '2020-07-14 09:51:50', '', 0, 'http://xneelo.test/?post_type=news&#038;p=99', 0, 'news', '', 0),
(100, 1, '2020-07-14 09:51:09', '2020-07-14 09:51:09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Virus', '', 'inherit', 'closed', 'closed', '', '99-revision-v1', '', '', '2020-07-14 09:51:09', '2020-07-14 09:51:09', '', 99, 'http://xneelo.test/99-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2020-07-14 09:51:45', '2020-07-14 09:51:45', '', 'wolf-bell', '', 'inherit', 'open', 'closed', '', 'wolf-bell', '', '', '2020-07-14 09:51:45', '2020-07-14 09:51:45', '', 99, 'http://xneelo.test/wp-content/uploads/2020/07/wolf-bell.jpg', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(2, 'Current', 'current', 0),
(3, 'Pending', 'pending', 0),
(4, 'Completed', 'completed', 0),
(5, 'Marketing', 'marketing', 0),
(6, 'Programming', 'programming', 0),
(7, 'Design', 'design', 0),
(8, 'Primary', 'primary', 0),
(9, 'Logged nav', 'logged-nav', 0),
(10, 'test', 'test', 0),
(11, 'Programming', 'programming', 0),
(12, 'Design', 'design', 0),
(13, 'Marketing', 'marketing', 0),
(14, 'South Africa', 'south-africa', 0),
(15, 'Trouble', 'trouble', 0),
(16, 'Africa', 'africa', 0),
(17, 'War', 'war', 0),
(18, 'World', 'world', 0),
(19, 'disease', 'disease', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 7, 0),
(37, 4, 0),
(37, 7, 0),
(37, 12, 0),
(39, 2, 0),
(39, 6, 0),
(39, 11, 0),
(41, 3, 0),
(41, 5, 0),
(41, 13, 0),
(68, 8, 0),
(69, 8, 0),
(70, 8, 0),
(72, 8, 0),
(73, 8, 0),
(74, 8, 0),
(79, 9, 0),
(80, 9, 0),
(85, 14, 0),
(85, 15, 0),
(85, 16, 0),
(87, 15, 0),
(87, 16, 0),
(87, 17, 0),
(99, 18, 0),
(99, 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(2, 2, 'Status', '', 0, 0),
(3, 3, 'Status', '', 0, 0),
(4, 4, 'Status', '', 0, 1),
(5, 5, 'Category', '', 0, 0),
(6, 6, 'Category', '', 0, 0),
(7, 7, 'Category', '', 0, 0),
(8, 8, 'nav_menu', '', 0, 6),
(9, 9, 'nav_menu', '', 0, 2),
(10, 10, 'category', '', 0, 0),
(11, 11, 'course_category', '', 0, 0),
(12, 12, 'course_category', '', 0, 1),
(13, 13, 'course_category', '', 0, 0),
(14, 14, 'news_category', '', 0, 1),
(15, 15, 'Tag', '', 0, 2),
(16, 16, 'news_category', '', 0, 2),
(17, 17, 'Tag', '', 0, 1),
(18, 18, 'news_category', '', 0, 1),
(19, 19, 'Tag', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'MrTicklish'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"596af90f2896133dd5a11633344bb02e623f400131af63974fc66384db872dff\";a:4:{s:10:\"expiration\";i:1594891644;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:5:\"login\";i:1594718844;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse'),
(20, 1, 'wp_user-settings-time', '1594552228'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:20:\"add-post-type-course\";i:1;s:12:\"add-post_tag\";i:2;s:9:\"add-skill\";i:3;s:12:\"add-Duration\";i:4;s:12:\"add-Category\";}'),
(23, 1, 'nav_menu_recently_edited', '9');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'MrTicklish', '$P$Bh332Vb0qAEIWnEANe3Hovdk28IkxQ1', 'mrticklish', 'richard.klement@gmail.com', 'http://xneelo.test', '2020-07-12 09:06:18', '', 0, 'MrTicklish');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
